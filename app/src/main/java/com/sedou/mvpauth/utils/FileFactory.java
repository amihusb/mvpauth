package com.sedou.mvpauth.utils;

import android.content.Context;
import android.support.annotation.Nullable;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.os.Environment.DIRECTORY_PICTURES;
import static java.text.DateFormat.MEDIUM;

public class FileFactory {

    @Nullable
    public static File createFileForPhoto(Context context) {
        DateFormat dateFormatInstance = SimpleDateFormat.getTimeInstance(MEDIUM);
        String timeStamp = dateFormatInstance.format(new Date());
        String imageFileName = ConstantManager.PHOTO_FILE_PREFIX + timeStamp;
        File storageDir = context.getExternalFilesDir(DIRECTORY_PICTURES);
        File fileImage;
        try {
            fileImage = File.createTempFile(imageFileName, ".jpg", storageDir);
        } catch (IOException exception) {
            return null;
        }
        return fileImage;
    }
}
