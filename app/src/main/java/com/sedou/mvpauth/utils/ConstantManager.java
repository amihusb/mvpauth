package com.sedou.mvpauth.utils;

public class ConstantManager {

    public static final int REQUEST_PERMISSION_CAMERA = 1008;
    public static final int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 1077;

    public static final int REQUEST_PROFILE_PHOTO_PICKER = 2014;
    public static final int REQUEST_PROFILE_PHOTO_CAMERA = 2248;

    public static final String PHOTO_FILE_PREFIX = "IMG_";

    public static final String FILE_PROVIDER_AUTHORITY = "com.sedou.mvpauth.fileprovider";

    public static final String LAST_MODIFIED_HEADER = "Last-Modified";
    public static final String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";

}
