package com.sedou.mvpauth.data.network;

import com.sedou.mvpauth.data.network.res.ProductRes;
import com.sedou.mvpauth.utils.ConstantManager;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import rx.Observable;

import static com.sedou.mvpauth.utils.ConstantManager.IF_MODIFIED_SINCE_HEADER;

public interface RestService {
    @GET("products")
    Observable<Response<List<ProductRes>>> getProductResObs(@Header(IF_MODIFIED_SINCE_HEADER) String lastEntityUpdate);
}
