package com.sedou.mvpauth.data.storage.dto;

import android.content.Intent;
import android.support.annotation.Nullable;

public class ActivityResultDto {

    private int resultCode;
    private int requestCode;
    @Nullable
    private Intent data;

    public ActivityResultDto(int resultCode, int requestCode, Intent data) {
        this.resultCode = resultCode;
        this.requestCode = requestCode;
        this.data = data;
    }

    public int getResultCode() {
        return resultCode;
    }

    public int getRequestCode() {
        return requestCode;
    }

    @Nullable
    public Intent getIntent() {
        return data;
    }
}
