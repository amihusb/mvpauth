package com.sedou.mvpauth.data.storage.realm;

import com.sedou.mvpauth.data.network.res.CommentRes;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CommentRealm extends RealmObject {

    @PrimaryKey
    private String id;
    private String userName;
    private String avatar;
    private float rating;
    private Date commentDate;
    private String comment;

    // Необходимо для realm
    public CommentRealm() {
    }

    public CommentRealm(CommentRes commentRes) {
        id = commentRes.getId();
        userName = commentRes.getUserName();
        avatar = commentRes.getAvatar();
        rating = commentRes.getRating();
        commentDate = commentRes.getCommentDate();
        comment = commentRes.getComment();
    }

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public float getRating() {
        return rating;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public String getComment() {
        return comment;
    }
}
