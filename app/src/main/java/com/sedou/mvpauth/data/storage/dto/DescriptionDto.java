package com.sedou.mvpauth.data.storage.dto;

import com.sedou.mvpauth.data.storage.realm.ProductRealm;

public class DescriptionDto {

    private String fullDescription;
    private int count;
    private int price;
    private float rating;
    private boolean favorite;

    public DescriptionDto(ProductRealm product) {
        this.fullDescription = product.getDescription();
        this.count = product.getCount();
        this.price = product.getPrice();
        this.rating = product.getRating();
        this.favorite = product.isFavorite();
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public int getCount() {
        return count;
    }

    public int getPrice() {
        return price;
    }

    public float getRating() {
        return rating;
    }

    public boolean isFavorite() {
        return favorite;
    }
}
