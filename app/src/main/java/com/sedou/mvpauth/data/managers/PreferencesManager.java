package com.sedou.mvpauth.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.sedou.mvpauth.data.storage.dto.ProductLocalInfo;
import com.sedou.mvpauth.data.storage.dto.UserAddressDto;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

public class PreferencesManager {

    private static final String KEY_TOKEN = "KEY_TOKEN";

    public static final String PROFILE_FULL_NAME_KEY = "PROFILE_FULL_NAME_KEY";
    public static final String PROFILE_AVATAR_KEY = "PROFILE_AVATAR_KEY";
    public static final String PROFILE_PHONE_KEY = "PROFILE_PHONE_KEY";
    public static final String NOTIFICATION_ORDER_KEY = "NOTIFICATION_ORDER_KEY";
    public static final String NOTIFICATION_PROMO_KEY = "NOTIFICATION_PROMO_KEY";
    public static final String USER_ADDRESSES_KEY = "USER_ADDRESSES_KEY";
    public static final String PRODUCT_LAST_UPDATE_KEY = "PRODUCT_LAST_UPDATE_KEY";

    private final SharedPreferences mSharedPreferences;
    @SuppressWarnings("SpellCheckingInspection")
    private Moshi mMoshi;
    private JsonAdapter<UserAddressDto> mJsonAdapter;

    public PreferencesManager(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        mMoshi = new Moshi.Builder()
                .build();
        mJsonAdapter = mMoshi.adapter(UserAddressDto.class);
    }

    public String getLastProductUpdate(){
        return mSharedPreferences.getString(PRODUCT_LAST_UPDATE_KEY, "Thu, 01 Jan 1970 00:00:00 GMT");
    }

    public void saveLastProductUpdate(String lastModified){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PRODUCT_LAST_UPDATE_KEY, lastModified);
        editor.apply();
    }

    public void saveToken(String token) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_TOKEN, token);
        editor.apply();
    }

    public String getToken() {
        return mSharedPreferences.getString(KEY_TOKEN, "");
    }

    public ProductLocalInfo getLocalInfo(int remoteId) {
        return new ProductLocalInfo(remoteId, false, 1);
    }
}