package com.sedou.mvpauth.data.storage.dto;

public class ProductLocalInfo {
    private int remoteId;
    private boolean favorite;
    private int count;
    private int mRemoteId;

    public ProductLocalInfo(int remoteId, boolean favorite, int count) {
        this.remoteId = remoteId;
        this.favorite = favorite;
        this.count = count;
    }

    public ProductLocalInfo() {

    }

    public int getRemoteId() {
        return remoteId;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public int getCount() {
        return count;
    }

    public void incCount(){
        count++;
    }

    public void decCount(){
        count--;
    }

    public void setRemoteId(int remoteId) {
        mRemoteId = remoteId;
    }
}
