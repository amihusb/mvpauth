package com.sedou.mvpauth.data.storage.dto;

import com.sedou.mvpauth.data.storage.realm.CommentRealm;

public class CommentDto {

    private String userName;
    private String avatarUri;
    private float rating;
    private String commentDate;
    private String comment;

    public CommentDto(CommentRealm commentRealm) {
        this.avatarUri = commentRealm.getAvatar();
        this.rating = commentRealm.getRating();
        this.commentDate = commentRealm.getCommentDate().toString();
        this.comment = commentRealm.getComment();
        this.userName = commentRealm.getUserName();
    }

    public String getAvatarUri() {
        return avatarUri;
    }

    public float getRating() {
        return rating;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public String getComment() {
        return comment;
    }

    public String getUserName() {
        return userName;
    }
}
