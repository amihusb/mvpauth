package com.sedou.mvpauth.data.managers;

import com.sedou.mvpauth.data.network.res.CommentRes;
import com.sedou.mvpauth.data.network.res.ProductRes;
import com.sedou.mvpauth.data.storage.realm.CommentRealm;
import com.sedou.mvpauth.data.storage.realm.ProductRealm;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import rx.Observable;

public class RealmManager {

    private Realm mRealmInstance;

    public void saveProductResponseToRealm(ProductRes productRes){
        Realm realm = Realm.getDefaultInstance();

        ProductRealm productRealm = new ProductRealm(productRes);

        if(!productRes.getComments().isEmpty()){
            Observable.from(productRes.getComments())
                    .doOnNext(commentRes -> {
                        if(commentRes.isActive()){
                            deleteFromRealm(CommentRealm.class, commentRes.getId());
                        }
                    })
                    .filter(CommentRes::isActive)
                    .map(CommentRealm::new)
                    .subscribe(commentRealm -> productRealm.getCommentRealms().add(commentRealm));
        }

        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(productRealm));
        realm.close();
    }

    public void deleteFromRealm(Class<? extends RealmObject> entityRealmClass, String id) {
        Realm realm = Realm.getDefaultInstance();
        RealmObject entity = realm.where(entityRealmClass).equalTo("id", id).findFirst();

        if (entity!= null){
            realm.executeTransaction(realm1 -> entity.deleteFromRealm());
            realm.close();
        }
    }

    public Observable<ProductRealm> getAllProductsFromRealmObs(){
        RealmResults<ProductRealm> managedProduct = getQueryRealmInstance().where(ProductRealm.class).findAllAsync();
        return managedProduct
                .asObservable()
                .filter(RealmResults::isLoaded) //hot observable, т.е. нельзя получить лиск колеекции, она не завершается, будет иммитить при любом изменении
                .first() //if need cold observable
                .flatMap(Observable::from);
    }

    private Realm getQueryRealmInstance() {

        if (mRealmInstance == null || mRealmInstance.isClosed()){
            mRealmInstance = Realm.getDefaultInstance();
        }
        return mRealmInstance;
    }
}
