package com.sedou.mvpauth.data.network.res;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;

import io.realm.internal.android.ISO8601Utils;

public class DateJsonAdapter {

    @FromJson
    Date dateFromString(String dateString) {
        Date date = new Date();
        try {
            date = ISO8601Utils.parse(dateString, new ParsePosition(0));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    @ToJson
    String dateToString(Date date) {
        return date.toString();
    }
}
