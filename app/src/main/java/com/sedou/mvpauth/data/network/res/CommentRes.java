package com.sedou.mvpauth.data.network.res;

import com.squareup.moshi.Json;

import java.util.Date;

public class CommentRes {

    @Json(name = "_id")
    private String id;
    private String avatar;
    private String userName;
    @Json(name = "raiting")
    private float rating;
    private String comment;
    private Date commentDate;
    //private String commentDate;
    private boolean active;

    public String getId() {
        return id;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getUserName() {
        return userName;
    }

    public float getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public boolean isActive() {
        return active;
    }
}
