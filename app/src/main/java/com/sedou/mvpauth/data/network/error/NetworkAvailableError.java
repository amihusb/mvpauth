package com.sedou.mvpauth.data.network.error;

public class NetworkAvailableError extends Throwable {
    public NetworkAvailableError() {
        super("Интернет недоступен, попробуйте позже");
    }
}
