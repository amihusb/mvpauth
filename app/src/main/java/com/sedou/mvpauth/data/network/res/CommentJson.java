package com.sedou.mvpauth.data.network.res;

import com.squareup.moshi.Json;

public class CommentJson {
    public String id;
    public String remoteId;
    public String avatar;
    public String userName;
    @Json(name = "raiting")
    public int rating;
    public String commentDate;
    public String comment;
    public boolean active;

    public CommentJson(String id, String remoteId, String avatar, String userName, int rating, String commentDate, String comment, boolean active) {
        this.id = id;
        this.remoteId = remoteId;
        this.avatar = avatar;
        this.userName = userName;
        this.rating = rating;
        this.commentDate = commentDate;
        this.comment = comment;
        this.active = active;
    }
}
