package com.sedou.mvpauth.data.storage.dto;

public class UserSettingDto {

    private boolean mOrderNotification;
    private boolean mPromoNotification;

    public UserSettingDto(boolean orderNotification, boolean promoNotification) {
        mOrderNotification = orderNotification;
        mPromoNotification = promoNotification;
    }

    public boolean isOrderNotification() {
        return mOrderNotification;
    }

    public boolean isPromoNotification() {
        return mPromoNotification;
    }
}
