package com.sedou.mvpauth.data.managers;


import com.sedou.mvpauth.App;
import com.sedou.mvpauth.data.network.RestCallTransformer;
import com.sedou.mvpauth.data.network.RestService;
import com.sedou.mvpauth.data.network.res.ProductRes;
import com.sedou.mvpauth.data.storage.dto.ProductDto;
import com.sedou.mvpauth.data.storage.dto.UserAddressDto;
import com.sedou.mvpauth.data.storage.realm.ProductRealm;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.di.components.DaggerDataManagerComponent;
import com.sedou.mvpauth.di.components.DataManagerComponent;
import com.sedou.mvpauth.di.modules.LocalModule;
import com.sedou.mvpauth.di.modules.NetworkModule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;
import rx.schedulers.Schedulers;

public class DataManager {

    private static DataManager ourInstance = new DataManager();

    @Inject
    PreferencesManager mPreferencesManager;
    @Inject
    RestService mRestService;
    @Inject
    Retrofit mRetrofit;
    @Inject
    RealmManager mRealmManager;

    private List<ProductDto> mMockProductList;

    private DataManager() {
        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(DataManagerComponent.class, component);
        }
        component.inject(this);
        generateMockData();
    }

    public static DataManager getInstance() {
        return ourInstance;
    }

    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public Observable<ProductRealm> getProductsObsFromNetwork() {
        return mRestService.getProductResObs(mPreferencesManager.getLastProductUpdate())
                .compose(new RestCallTransformer<List<ProductRes>>())//трансформируем response, выбрасываем ApiError в случае ошибки, проверяем статус сети перед запросом, обрабатываем коды ответа
                .flatMap(Observable::from) //преобразуем список товаров в последовательность товаров
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .doOnNext(productRes -> {
                    if (!productRes.isActive()) {
                        mRealmManager.deleteFromRealm(ProductRealm.class, productRes.getId()); //удалить из бд запись о продукте, если он больше не активен
                    }
                })
                .filter(ProductRes::isActive) //оставляем только активные
                .doOnNext(productRes -> mRealmManager.saveProductResponseToRealm(productRes)) //сохраняем на диск только активные
                .flatMap(productRes -> Observable.empty());
    }

    private void deleteFromDb(ProductRes productRes) {
        // TODO: 08.01.2017 with realm
    }

    private void saveOnDick(ProductRes productRes) {
        // TODO: 08.01.2017 with realm
    }

    private DataManagerComponent createDaggerComponent() {
        return DaggerDataManagerComponent
                .builder()
                .appComponent(App.getAppComponent())
                .localModule(new LocalModule())
                .networkModule(new NetworkModule())
                .build();
    }

    public ProductDto getProductById(int productId) {
        // TODO: 27.10.2016 this temp sample mock data fix
        return mMockProductList.get(productId);
    }

    public void updateProduct(ProductDto product) {
        // TODO: 27.10.2016 update product count or status save in DB
    }

    public List<ProductDto> getMockProductList() {
        return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(0, "Pig 1", "https://pp.vk.me/c637818/v637818635/1f14b/T_-GjGyVC9g.jpg", "Best Pig In The World!!!", 9999, 1));
        mMockProductList.add(new ProductDto(1, "Pig 2", "https://pp.vk.me/c637818/v637818635/1f14b/T_-GjGyVC9g.jpg", "Best Pig In The World!!!", 9999, 1));
        mMockProductList.add(new ProductDto(2, "Pig 3", "https://pp.vk.me/c637818/v637818635/1f14b/T_-GjGyVC9g.jpg", "Best Pig In The World!!!", 9999, 1));
    }

    private Map<String, String> generateMockUserProfileInfo() {
        Map<String, String> map = new HashMap<>();
        map.put(PreferencesManager.PROFILE_FULL_NAME_KEY, "Ivan");
        map.put(PreferencesManager.PROFILE_PHONE_KEY, "+7(919)9327765");
        map.put(PreferencesManager.PROFILE_AVATAR_KEY, "https://pp.vk.me/c637819/v637819635/1a2bc/B1Tmee4ilG0.jpg");
        return map;
    }

    private List<UserAddressDto> generateMockUserAddresses() {
        List<UserAddressDto> list = new ArrayList<>();
        list.add(new UserAddressDto(0, "Работа", "Дзержинского", "15", "", 4, "3 дверь"));
        list.add(new UserAddressDto(1, "Дом", "Нагорная", "6", "221", 2, "Комната 221б"));
        return list;
    }

    private Map<String, Boolean> generateMockUserSettings() {
        Map<String, Boolean> map = new HashMap<>();
        map.put(PreferencesManager.NOTIFICATION_ORDER_KEY, true);
        map.put(PreferencesManager.NOTIFICATION_PROMO_KEY, true);
        return map;
    }

    public boolean isUserAuth() {
        // TODO: 28.10.2016 check user auth token is shared preferences
        return false;
    }

    public Map<String, String> getUserProfileInfo() {
        return generateMockUserProfileInfo();
    }

    public List<UserAddressDto> getUserAddresses() {
        return generateMockUserAddresses();
    }

    public Map<String, Boolean> getUserSettings() {
        return generateMockUserSettings();
    }

    public void saveSettings(String notificationPromoKey, boolean isChecked) {
        // TODO: 04.12.2016 implement this
    }

    public void addAddress(UserAddressDto userAddressDto) {
        // TODO: 04.12.2016 implement this
    }

    public void updateOrInsertAddress(UserAddressDto addressDto) {
        // TODO: 03.01.2017 implement this
    }

    public void removeAddress(UserAddressDto addressDto) {
        // TODO: 03.01.2017 implement this
    }

    public void saveProfileInfo(String name, String phone, String avatar) {
        // TODO: 04.01.2017 imp me plz
    }

    public List<ProductDto> fromDisk() {
        return mMockProductList;
    }

    /*public Observable<ProductLocalInfo> getProductsLocalInfoObs(ProductRes productRes) {
        return Observable.just(getPreferencesManager().getLocalInfo(productRes.getRemoteId()))
                .flatMap(productLocalInfo ->
                        productLocalInfo == null ?
                                Observable.just(new ProductLocalInfo()) :
                                Observable.just(productLocalInfo)
                );
    }*/

    public Observable<ProductRealm> getProductObsFromRealm() {
        return mRealmManager.getAllProductsFromRealmObs();
    }
}