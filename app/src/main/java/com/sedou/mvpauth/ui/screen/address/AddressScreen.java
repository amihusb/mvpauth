package com.sedou.mvpauth.ui.screen.address;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.dto.UserAddressDto;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.di.scopes.AddressScope;
import com.sedou.mvpauth.flow.AbstractScreen;
import com.sedou.mvpauth.flow.Screen;
import com.sedou.mvpauth.mvp.models.AccountModel;
import com.sedou.mvpauth.mvp.presenters.IAddressPresenter;
import com.sedou.mvpauth.ui.screen.account.AccountScreen;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import flow.TreeKey;
import mortar.MortarScope;
import mortar.ViewPresenter;

@Screen(R.layout.screen_add_address)
public class AddressScreen extends AbstractScreen<AccountScreen.Component> implements TreeKey {

    @Nullable
    private UserAddressDto mAddressDto;

    public AddressScreen(@Nullable UserAddressDto addressDto) {
        mAddressDto = addressDto;
    }

    @Override
    public boolean equals(Object o) {
        return mAddressDto != null ?
                o instanceof AddressScreen && mAddressDto.equals(((AddressScreen) o).mAddressDto) :
                super.equals(o);
    }

    @Override
    public int hashCode() {
        return mAddressDto != null ?
                mAddressDto.hashCode() :
                super.hashCode();
    }

    @Override
    public Object createScreenComponent(AccountScreen.Component parentComponent) {
        return DaggerAddressScreen_Component
                .builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new AccountScreen();
    }


    //region DI

    @dagger.Module
    public class Module {

        @Provides
        @AddressScope
        AddressPresenter provideAddressPresenter() {
            return new AddressPresenter();
        }
    }

    @dagger.Component(dependencies = AccountScreen.Component.class, modules = Module.class)
    @AddressScope
    interface Component {
        void inject(AddressPresenter presenter);

        void inject(AddressView view);
    }
    //endregion

    //region Presenter

    public class AddressPresenter extends ViewPresenter<AddressView> implements IAddressPresenter {

        @Inject
        AccountModel mAccountModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (mAddressDto != null && getView() != null) {
                getView().initView(mAddressDto);
            }
        }

        @Override
        public void clickOnAddAddress() {
            if (getView() != null) {
                mAccountModel.updateOrInsertAddress(getView().getUserAddress());
                Flow.get(getView()).goBack();
            }
        }
    }
    //endregion
}
