package com.sedou.mvpauth.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sedou.mvpauth.BuildConfig;
import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.dto.UserInfoDto;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.di.components.AppComponent;
import com.sedou.mvpauth.di.modules.PicassoCacheModule;
import com.sedou.mvpauth.di.modules.RootModule;
import com.sedou.mvpauth.di.scopes.RootScope;
import com.sedou.mvpauth.flow.TreeKeyDispatcher;
import com.sedou.mvpauth.mvp.models.AccountModel;
import com.sedou.mvpauth.mvp.presenters.MenuItemHolder;
import com.sedou.mvpauth.mvp.presenters.RootPresenter;
import com.sedou.mvpauth.mvp.views.IActionBarView;
import com.sedou.mvpauth.mvp.views.IRootView;
import com.sedou.mvpauth.mvp.views.IView;
import com.sedou.mvpauth.ui.screen.account.AccountScreen;
import com.sedou.mvpauth.ui.screen.catalog.CatalogScreen;
import com.sedou.mvpauth.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class RootActivity extends BaseActivity implements IRootView, NavigationView.OnNavigationItemSelectedListener, IActionBarView {

    private boolean mDoubleBackToExitPressedOnce = false;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;
    @BindView(R.id.appbar_layout)
    AppBarLayout mAppbarLayout;

    private TextView mCartCount;

    @Inject
    RootPresenter mRootPresenter;
    @Inject
    Picasso mPicasso;

    private ActionBarDrawerToggle mToggle;
    private ActionBar mActionBar;
    private List<MenuItemHolder> mActionBarMenuItems;

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new CatalogScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        ButterKnife.bind(this);
        RootComponent rootComponent = DaggerService.getDaggerComponent(this);
        rootComponent.inject(this);

        initToolbar();
        initDrawer();
        mRootPresenter.takeView(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView(this);
        super.onDestroy();
    }

    private void initDrawer() {
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Object key = null;
        switch (item.getItemId()) {
            case R.id.nav_account:
                key = new AccountScreen();
                break;
            case R.id.nav_catalog:
                key = new CatalogScreen();
                break;
            case R.id.nav_favourite:
                break;
            case R.id.nav_notification:
                break;
            case R.id.nav_orders:
                break;
        }

        if (key != null) {
            Flow.get(this).set(key);
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_root_menu, menu);
        MenuItem item = menu.findItem(R.id.cart_menu);
        MenuItemCompat.setActionView(item, R.layout.bag_with_count);
        RelativeLayout relativeLayout = (RelativeLayout) MenuItemCompat.getActionView(item);
        mCartCount = (TextView) relativeLayout.findViewById(R.id.bag_count_txt);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && Flow.get(this).goBack()) {
            if (mDoubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.mDoubleBackToExitPressedOnce = true;
            showMessage("Нажмите еще раз для выхода");

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mDoubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mRootPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mRootPresenter.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    //region IRootView
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.root_unknown_error));
            // TODO: 20.10.2016 send error stacktrace ro crashlist
        }
    }

    @Override
    public void showLoad() {
        showLoadProgress();
    }

    @Override
    public void hideLoad() {
        hideLoadProgress();
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }

    @Override
    public void initDrawer(UserInfoDto userInfoDto) {
        View header = mNavigationView.getHeaderView(0);
        ImageView avatar = (ImageView) header.findViewById(R.id.drawer_user_avatar_img);
        TextView userName = (TextView) header.findViewById(R.id.drawer_user_name_txt);
        mPicasso.load(Uri.parse(userInfoDto.getAvatar()))
                .resize(60, 60)
                .centerCrop()
                .transform(new CircleTransform())
                .into(avatar);
        userName.setText(userInfoDto.getName());
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion

    public TextView getCartCount() {
        return mCartCount;
    }

    //region ActionBar

    @Override
    public void setVisable(boolean visible) {
        mToolbar.setVisibility(visible ? VISIBLE : GONE);
    }

    @Override
    public void setBackArrow(boolean enabled) {
        if (mToggle == null || mActionBar == null) return;

        if (enabled) {
            mToggle.setDrawerIndicatorEnabled(false);
            mActionBar.setDisplayHomeAsUpEnabled(true);
            if (mToggle.getToolbarNavigationClickListener() == null) {
                mToggle.setToolbarNavigationClickListener(v -> onBackPressed());
            }
        } else {
            mActionBar.setDisplayHomeAsUpEnabled(false);
            mToggle.setDrawerIndicatorEnabled(true);
            mToggle.setToolbarNavigationClickListener(null);
        }

        mDrawerLayout.setDrawerLockMode(enabled ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_UNLOCKED);
        mToggle.syncState();
    }

    @Override
    public void setMenuItem(List<MenuItemHolder> items) {
        mActionBarMenuItems = items;
        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mActionBarMenuItems != null) {
            for (MenuItemHolder menuItem : mActionBarMenuItems) {
                MenuItem item = menu.add(menuItem.getTitle());
                item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)
                        .setIcon(menuItem.getIconResId())
                        .setOnMenuItemClickListener(menuItem.getListener());
            }
        } else {
            menu.clear();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTabLayout(ViewPager pager) {
        TabLayout tabView = new TabLayout(this);
        tabView.setupWithViewPager(pager);
        // TODO: 19.01.2017 try to customize tab layout ? растянуть табы на всю ширину
        mAppbarLayout.addView(tabView);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabView));
    }

    @Override
    public void removeTabLayout() {
        View tabView = mAppbarLayout.getChildAt(1); // первый toolbar, второй tab layout
        if (tabView != null && tabView instanceof TabLayout){
            mAppbarLayout.removeView(tabView);
        }
    }

    //endregion

    //region DI

    @dagger.Component(dependencies = AppComponent.class, modules = {RootModule.class, PicassoCacheModule.class})
    @RootScope
    public interface RootComponent {
        void inject(RootActivity activity);

        void inject(SplashActivity activity);

        void inject(RootPresenter presenter);

        AccountModel getAccountModel();

        RootPresenter getRootPresenter();

        Picasso getPicasso();
    }
    //endregion
}