package com.sedou.mvpauth.ui.screen.product;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.dto.ProductDto;
import com.sedou.mvpauth.data.storage.dto.ProductLocalInfo;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.flow.Screen;
import com.sedou.mvpauth.mvp.views.AbstractView;
import com.sedou.mvpauth.mvp.views.IProductView;
import com.sedou.mvpauth.ui.custom_views.AspectRatioImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

@Screen(R.layout.screen_product)
public class ProductView extends AbstractView<ProductScreen.ProductPresenter> implements IProductView {

    @BindView(R.id.product_name_txt)
    TextView mProductName;
    @BindView(R.id.product_description_txt)
    TextView mProductDescription;
    @BindView(R.id.product_image)
    AspectRatioImageView mProductImage;
    @BindView(R.id.product_count_txt)
    TextView mProductCount;
    @BindView(R.id.product_price_txt)
    TextView mProductPrice;
    @BindView(R.id.favorite_btn)
    CheckBox mFavoriteBtn;
    @BindView(R.id.show_more_btn)
    Button mShowMoreBtn;

    @Inject
    Picasso mPicasso;

    public ProductView(Context context) {
        this(context, null);
    }

    public ProductView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProductView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void initializeDagger(Context context) {
        DaggerService.<ProductScreen.Component>getDaggerComponent(context).inject(this);
    }

    //region IProductView
    @Override
    public void showProductView(final ProductDto product) {
        mProductName.setText(product.getProductName());
        mProductDescription.setText(product.getDescription());
        mProductCount.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPrice.setText(getContext().getResources().getString(R.string.price, product.getCount() * product.getPrice()));
        } else {
            mProductPrice.setText(getContext().getResources().getString(R.string.price, product.getPrice()));
        }

        mPicasso.load(product.getImageUrl())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .fit()
                .centerCrop()
                .into(mProductImage, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                        mPicasso.load(product.getImageUrl())
                                .fit()
                                .centerCrop()
                                .into(mProductImage);
                    }
                });
    }

    public ProductLocalInfo getProductLocalInfo(){
        return new ProductLocalInfo(0, mFavoriteBtn.isChecked(), Integer.parseInt(mProductCount.getText().toString()));
    }

    @Override
    public void updateProductCountView(ProductDto product) {
        mProductCount.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPrice.setText(getContext().getResources().getString(R.string.price, product.getCount() * product.getPrice()));
        }
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }
    //endregion

    //region Events

    @OnClick(R.id.plus_btn)
    void clickPlus(){
        mPresenter.clickOnPlus();
    }

    @OnClick(R.id.minus_btn)
    void clickMinus(){
        mPresenter.clickOnMinus();
    }

    @OnClick(R.id.favorite_btn)
    void clickOnFavorite(){ mPresenter.clickOnFavorite();}

    @OnClick(R.id.show_more_btn)
    void clickShowMore() {mPresenter.clickOnShowMore();}

    //endregion
}
