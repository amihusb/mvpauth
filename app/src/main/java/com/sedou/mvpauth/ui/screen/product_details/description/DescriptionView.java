package com.sedou.mvpauth.ui.screen.product_details.description;

import android.content.Context;
import android.support.v7.widget.AppCompatRatingBar;
import android.util.AttributeSet;
import android.widget.TextView;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.dto.DescriptionDto;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.mvp.views.AbstractView;

import butterknife.BindView;
import butterknife.OnClick;

public class DescriptionView extends AbstractView<DescriptionScreen.DescriptionPresenter> {

    @BindView(R.id.full_description_txt)
    TextView mFullDescriptionTxt;
    @BindView(R.id.product_count_txt)
    TextView mProductCountTxt;
    @BindView(R.id.product_price_txt)
    TextView mProductPriceTxt;
    @BindView(R.id.product_rating)
    AppCompatRatingBar mProductRating;

    public DescriptionView(Context context) {
        this(context, null);
    }

    public DescriptionView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DescriptionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void initializeDagger(Context context) {
        DaggerService.<DescriptionScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    public void initView(DescriptionDto descriptionDto) {
        mFullDescriptionTxt.setText(descriptionDto.getFullDescription());
        mProductCountTxt.setText(String.valueOf(descriptionDto.getCount()));
        mProductRating.setRating(descriptionDto.getRating());

        if (descriptionDto.getCount() > 0) {
            mProductPriceTxt.setText(getContext().getString(R.string.price, descriptionDto.getPrice() * descriptionDto.getCount()));
        } else {
            mProductPriceTxt.setText(getContext().getString(R.string.price, descriptionDto.getPrice()));
        }
    }

    //region Events

    @OnClick(R.id.plus_btn)
    void clickOnPlus(){
        mPresenter.clickOnPlus();
    }

    @OnClick(R.id.minus_btn)
    void clickOnMinus(){
        mPresenter.clickOnMinus();
    }

    //endregion
}
