package com.sedou.mvpauth.ui.screen.product_details.comments;

import android.net.Uri;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.dto.CommentDto;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentAdapter extends Adapter<CommentAdapter.CommentViewHolder> {

    private List<CommentDto> mCommentList = new ArrayList<>();

    @Inject
    Picasso mPicasso;

    public void addComment(CommentDto comment) {
        mCommentList.add(comment);
        notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        DaggerService.<CommentScreen.Component>getDaggerComponent(recyclerView.getContext()).inject(this);
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View commentView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comment, parent, false);
        return new CommentViewHolder(commentView);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        CommentDto comment = mCommentList.get(position);
        holder.mUserNameTxt.setText(comment.getUserName());
        holder.mCommentDateTxt.setText(comment.getCommentDate());
        holder.mCommentTxt.setText(comment.getComment());
        holder.mRating.setRating(comment.getRating());
        mPicasso.load(Uri.parse(comment.getAvatarUri()))
                .fit()
                .transform(new CircleTransform())
                .placeholder(R.drawable.ic_account)
                .into(holder.mCommentAvatarImg);

    }

    @Override
    public int getItemCount() {
        return mCommentList.size();
    }

    public class CommentViewHolder extends ViewHolder {

        @BindView(R.id.comment_avatar_img)
        ImageView mCommentAvatarImg;
        @BindView(R.id.user_name_txt)
        TextView mUserNameTxt;
        @BindView(R.id.comment_date_txt)
        TextView mCommentDateTxt;
        @BindView(R.id.comment_txt)
        TextView mCommentTxt;
        @BindView(R.id.rating)
        AppCompatRatingBar mRating;

        public CommentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
