package com.sedou.mvpauth.ui.behaviors;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.CoordinatorLayout.Behavior;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.sedou.mvpauth.R;

public class UserAvatarBehavior extends Behavior<ImageView> {

    private int mMaxAppBarHeight;
    private int mMaxAvatarMargin;

    public UserAvatarBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);

        mMaxAppBarHeight = context.getResources().getDimensionPixelSize(R.dimen.account_screen_app_bar_height);
        mMaxAvatarMargin = context.getResources().getDimensionPixelSize(R.dimen.account_scrren_avatar_margin);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, ImageView child, View dependency) {
        return dependency instanceof AppBarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, ImageView child, View dependency) {

        final float toolbarFriction = dependency.getBottom() * 1.0f / mMaxAppBarHeight * 1.0f;

        final int avatarMargin = mMaxAvatarMargin - (mMaxAppBarHeight - dependency.getBottom());

        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) child.getLayoutParams();
        layoutParams.topMargin = avatarMargin;
        child.setLayoutParams(layoutParams);
        child.setScaleX(toolbarFriction);
        child.setScaleY(toolbarFriction);

        return super.onDependentViewChanged(parent, child, dependency);
    }
}
