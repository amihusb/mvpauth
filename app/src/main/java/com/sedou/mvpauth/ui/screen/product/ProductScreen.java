package com.sedou.mvpauth.ui.screen.product;

import android.os.Bundle;

import com.sedou.mvpauth.data.storage.dto.ProductDto;
import com.sedou.mvpauth.data.storage.realm.ProductRealm;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.di.scopes.ProductScope;
import com.sedou.mvpauth.flow.AbstractScreen;
import com.sedou.mvpauth.mvp.models.CatalogModel;
import com.sedou.mvpauth.mvp.presenters.AbstractPresenter;
import com.sedou.mvpauth.mvp.presenters.IProductPresenter;
import com.sedou.mvpauth.ui.screen.catalog.CatalogScreen;
import com.sedou.mvpauth.ui.screen.product_details.DetailScreen;

import dagger.Provides;
import flow.Flow;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import mortar.MortarScope;

public class ProductScreen extends AbstractScreen<CatalogScreen.Component> {

    private ProductRealm mProductRealm;

    public ProductScreen(ProductRealm product) {
        mProductRealm = product;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ProductScreen && mProductRealm.equals(((ProductScreen) o).mProductRealm);
    }

    @Override
    public int hashCode() {
        return mProductRealm.hashCode();
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerProductScreen_Component
                .builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    //region DI

    @dagger.Module
    public class Module {

        @Provides
        @ProductScope
        ProductPresenter provideProductPresenter() {
            return new ProductPresenter(mProductRealm);
        }
    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    @ProductScope
    public interface Component {
        void inject(ProductPresenter presenter);

        void inject(ProductView view);
    }
    //endregion

    //region Presenter

    public class ProductPresenter extends AbstractPresenter<ProductView, CatalogModel> implements IProductPresenter {

        private ProductRealm mProduct;
        private RealmChangeListener mListener;

        public ProductPresenter(ProductRealm productRealm) {
            mProduct = productRealm;
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null && mProduct.isValid()) {
                getView().showProductView(new ProductDto(mProduct));
                mListener = o -> {
                    if (getView() != null) getView().showProductView(new ProductDto(mProduct));
                };
                mProduct.addChangeListener(mListener);
            } else {
                // TODO: 21.01.2017 impl me
            }
        }

        @Override
        public void dropView(ProductView view) {
            mProduct.removeChangeListener(mListener);
            super.dropView(view);
        }

        @Override
        protected void initializeDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initializeActionBar() {
            //empty Action Bar
        }

        @Override
        public void clickOnPlus() {
            if (getView() != null) {

                Realm realm = Realm.getDefaultInstance();
                realm.executeTransaction(realm1 -> mProduct.add());
                realm.close();
            }
        }

        @Override
        public void clickOnMinus() {
            if (getView() != null) {
                if (mProduct.getCount() > 0) {
                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(realm1 -> mProduct.remove());
                    realm.close();
                }
            }
        }

        public void clickOnFavorite() {
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(realm1 -> mProduct.changeFavorite());
            realm.close();
        }

        public void clickOnShowMore() {
            Flow.get(getView()).set(new DetailScreen(mProduct));
        }
    }

    //endregion
}
