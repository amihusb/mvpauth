package com.sedou.mvpauth.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.widget.FrameLayout;

import com.sedou.mvpauth.BuildConfig;
import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.dto.UserInfoDto;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.flow.TreeKeyDispatcher;
import com.sedou.mvpauth.mortar.ScreenScoper;
import com.sedou.mvpauth.mvp.presenters.RootPresenter;
import com.sedou.mvpauth.mvp.views.IRootView;
import com.sedou.mvpauth.mvp.views.IView;
import com.sedou.mvpauth.ui.screen.auth.AuthScreen;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class SplashActivity extends BaseActivity implements IRootView{

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;

    @Inject
    RootPresenter mRootPresenter;

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new AuthScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(@NonNull String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    //region Life Cycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        ButterKnife.bind(this);

        DaggerService.<RootActivity.RootComponent>getDaggerComponent(this).inject(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        mRootPresenter.dropView(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mRootPresenter.takeView(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()){
            ScreenScoper.destroyScreenScope(AuthScreen.class.getName());
        }
        super.onDestroy();
    }

    //endregion

    //region IAuthView

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.root_unknown_error));
            // TODO: 20.10.2016 send error stacktrace ro crashlist
        }
    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }

    @Override
    public void initDrawer(UserInfoDto userInfoDto) {

    }

    //endregion


    @Override
    public void onBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && Flow.get(this).goBack()){
            super.onBackPressed();
        }
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    public void startRootActivity(){
        Intent intent = new Intent(this, RootActivity.class);
        startActivity(intent);
        finish();
    }
}
