package com.sedou.mvpauth.ui.screen.product_details.comments;

import android.os.Bundle;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.dto.CommentDto;
import com.sedou.mvpauth.data.storage.realm.CommentRealm;
import com.sedou.mvpauth.data.storage.realm.ProductRealm;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.di.scopes.DaggerScope;
import com.sedou.mvpauth.flow.AbstractScreen;
import com.sedou.mvpauth.flow.Screen;
import com.sedou.mvpauth.mvp.models.DetailModel;
import com.sedou.mvpauth.mvp.presenters.AbstractPresenter;
import com.sedou.mvpauth.ui.screen.product_details.DetailScreen;

import dagger.Provides;
import io.realm.RealmList;
import mortar.MortarScope;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

@Screen(R.layout.screen_comments)
@SuppressWarnings("WeakerAccess")
public class CommentScreen extends AbstractScreen<DetailScreen.Component> {

    private ProductRealm mProductRealm;

    public CommentScreen(ProductRealm productRealm) {
        mProductRealm = productRealm;
    }

    @Override
    public Object createScreenComponent(DetailScreen.Component parentComponent) {
        return DaggerCommentScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    //region DI

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(CommentScreen.class)
        CommentPresenter provideCommentPresenter() {
            return new CommentPresenter(mProductRealm);
        }
    }

    @dagger.Component(dependencies = DetailScreen.Component.class, modules = Module.class)
    @DaggerScope(CommentScreen.class)
    public interface Component {
        void inject(CommentPresenter presenter);

        void inject(CommentView view);

        void inject(CommentAdapter adapter);
    }


    //endregion

    public class CommentPresenter extends AbstractPresenter<CommentView, DetailModel> {

        private final ProductRealm mProduct;

        public CommentPresenter(ProductRealm productRealm) {
            mProduct = productRealm;
        }

        @Override
        protected void initializeDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initializeActionBar() {
            //no action bar
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            RealmList<CommentRealm> comments = mProduct.getCommentRealms();
            Observable<CommentDto> commentsObs = Observable.from(comments)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .map(CommentDto::new);

            mCompositeSubscription.add(subscribe(commentsObs, new ViewSubscriber<CommentDto>() {
                @Override
                public void onNext(CommentDto commentDto) {
                    getView().getAdapter().addComment(commentDto);
                }
            }));
            getView().initView();
        }
    }
}

