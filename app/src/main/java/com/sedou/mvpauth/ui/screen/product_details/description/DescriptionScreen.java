package com.sedou.mvpauth.ui.screen.product_details.description;

import android.os.Bundle;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.dto.DescriptionDto;
import com.sedou.mvpauth.data.storage.realm.ProductRealm;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.di.scopes.DaggerScope;
import com.sedou.mvpauth.flow.AbstractScreen;
import com.sedou.mvpauth.flow.Screen;
import com.sedou.mvpauth.mvp.models.DetailModel;
import com.sedou.mvpauth.mvp.presenters.AbstractPresenter;
import com.sedou.mvpauth.ui.screen.product_details.DetailScreen;

import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import mortar.MortarScope;

@Screen(R.layout.screen_description)
public class DescriptionScreen extends AbstractScreen<DetailScreen.Component> {

    private ProductRealm mProductRealm;

    public DescriptionScreen(ProductRealm productRealm) {
        mProductRealm = productRealm;
    }

    @Override
    public Object createScreenComponent(DetailScreen.Component parentComponent) {
        return DaggerDescriptionScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    //region DI

    @dagger.Module
    public class Module {

        @Provides
        @DaggerScope(DescriptionScreen.class)
        DescriptionPresenter provideDescriptionPresenter() {
            return new DescriptionPresenter(mProductRealm);
        }
    }

    @dagger.Component(dependencies = DetailScreen.Component.class, modules = Module.class)
    @DaggerScope(DescriptionScreen.class)
    public interface Component {
        void inject(DescriptionPresenter presenter);
        void inject(DescriptionView view);
    }

    //endregion

    public class DescriptionPresenter extends AbstractPresenter<DescriptionView, DetailModel> {

        private final ProductRealm mProduct;
        private RealmChangeListener mListener;

        public DescriptionPresenter(ProductRealm productRealm) {
            mProduct = productRealm;
        }

        @Override
        protected void initializeDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initializeActionBar() {
            //no action bar
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().initView(new DescriptionDto(mProduct));

            mListener = o -> {
                if (getView() != null) {
                    getView().initView(new DescriptionDto(mProduct));
                }
            };

            mProduct.addChangeListener(mListener);
        }

        @Override
        public void dropView(DescriptionView view) {
            mProduct.removeChangeListener(mListener);
            super.dropView(view);
        }

        public void clickOnPlus(){
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(realm1 -> {
                mProduct.add();
            });
            realm.close();
        }

        public void clickOnMinus(){
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(realm1 -> {
                mProduct.remove();
            });
            realm.close();
        }
    }
}
