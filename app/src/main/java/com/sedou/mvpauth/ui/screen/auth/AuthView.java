package com.sedou.mvpauth.ui.screen.auth;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.mvp.views.IAuthView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;

public class AuthView extends RelativeLayout implements IAuthView {

    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;

    @Inject
    AuthScreen.AuthPresenter mPresenter;

    @BindView(R.id.auth_card)
    CardView mAuthCard;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    @BindView(R.id.login_email_et)
    EditText mEmailEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;

    private AuthScreen mScreen;

    public AuthView(Context context) {
        this(context, null);
    }

    public AuthView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AuthView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()){
            mScreen = Flow.getKey(this);
            DaggerService.<AuthScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

        if (!isInEditMode()){
            showViewFromState();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    private void showViewFromState() {
        if (mScreen.getCustomState() == LOGIN_STATE){
            showLoginState();
        } else {
            showIdleState();
        }
    }

    private void showLoginState() {
        // TODO: 30.11.2016 animation
        mAuthCard.setVisibility(VISIBLE);
        mShowCatalogBtn.setVisibility(GONE);
    }

    private void showIdleState() {
        // TODO: 30.11.2016 animation
        mAuthCard.setVisibility(GONE);
        mShowCatalogBtn.setVisibility(VISIBLE);
    }

    //region Events
    @OnClick(R.id.login_btn)
    void loginClick(){
        mPresenter.clickOnLogin();
    }

    @OnClick(R.id.show_catalog_btn)
    void catalogClick(){
        mPresenter.clickOnShowCatalog();
    }

    @OnClick(R.id.fb_social_btn)
    void fbClick(){
        mPresenter.clickOnFb();
    }

    @OnClick(R.id.vk_social_btn)
    void vkClick(){
        mPresenter.clickOnVk();
    }

    @OnClick(R.id.twitter_social_btn)
    void twitterClick(){
        mPresenter.clickOnTwitter();
    }
    //endregion
    //region IAuthView

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(GONE);
    }

    @Override
    public void showCatalogScreen() {
        mPresenter.clickOnShowCatalog();
    }

    @Override
    public String getUserEmail() {
        return String.valueOf(mEmailEt.getText());
    }

    @Override
    public String getUserPassword() {
        return String.valueOf(mPasswordEt.getText());
    }

    @Override
    public boolean isIdle() {
        return mScreen.getCustomState() == IDLE_STATE;
    }

    @Override
    public void setCustomState(int state) {
        mScreen.setCustomState(state);
        showViewFromState();
    }

    @Override
    public boolean viewOnBackPressed() {
        if (!isIdle()){
            setCustomState(IDLE_STATE);
            return true;
        } else {
            return false;
        }
    }
    //endregion
}
