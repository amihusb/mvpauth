package com.sedou.mvpauth.ui.screen.catalog;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.Button;

import com.rd.PageIndicatorView;
import com.sedou.mvpauth.R;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.mvp.views.AbstractView;
import com.sedou.mvpauth.mvp.views.ICatalogView;

import butterknife.BindView;
import butterknife.OnClick;

public class CatalogView extends AbstractView<CatalogScreen.CatalogPresenter> implements ICatalogView {

    @BindView(R.id.add_to_cart_btn)
    Button mAddToCardBtn;
    @BindView(R.id.product_pager)
    ViewPager mProductPager;
    @BindView(R.id.page_indicator)
    PageIndicatorView mPageIndicatorView;
    private CatalogAdapter mAdapter;

    public CatalogView(Context context) {
        this(context, null);
    }

    public CatalogView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CatalogView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void initializeDagger(Context context) {
        DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
        mAdapter = new CatalogAdapter();
    }

    @Override
    public void showCatalogView() {
        mProductPager.setAdapter(mAdapter);
        mPageIndicatorView.setViewPager(mProductPager);
    }

    @Override
    public void updateProductCounter() {
        // TODO: 28.10.2016 update count product on cart icon
    }

    public int getCurrentPagerPosition(){
        return mProductPager.getCurrentItem();
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @OnClick(R.id.add_to_cart_btn)
    void clickAddToCart(){
        mPresenter.clickOnBuyButton(mProductPager.getCurrentItem());
    }

    public CatalogAdapter getAdapter() {
        return mAdapter;
    }
}
