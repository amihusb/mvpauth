package com.sedou.mvpauth.ui.screen.product_details.comments;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.mvp.views.AbstractView;

import butterknife.BindView;

public class CommentView extends AbstractView<CommentScreen.CommentPresenter> {

    private CommentAdapter mAdapter = new CommentAdapter();

    @BindView(R.id.comments_list)
    RecyclerView mCommentsList;

    public CommentView(Context context) {
        this(context, null);
    }

    public CommentView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CommentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void initializeDagger(Context context) {
        DaggerService.<CommentScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    public CommentAdapter getAdapter() {
        return mAdapter;
    }

    public void initView(){
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        mCommentsList.setLayoutManager(llm);
        mCommentsList.setAdapter(mAdapter);
    }
}
