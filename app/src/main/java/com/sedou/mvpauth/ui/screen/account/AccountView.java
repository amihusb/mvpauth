package com.sedou.mvpauth.ui.screen.account;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.dto.UserDto;
import com.sedou.mvpauth.data.storage.dto.UserInfoDto;
import com.sedou.mvpauth.data.storage.dto.UserSettingDto;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.mvp.views.IAccountView;
import com.sedou.mvpauth.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;

public class AccountView extends CoordinatorLayout implements IAccountView {

    public static final int EDIT_STATE = 0;
    public static final int PREVIEW_STATE = 1;

    @Inject
    AccountScreen.AccountPresenter mPresenter;
    @Inject
    Picasso mPicasso;

    @BindView(R.id.profile_name_txt)
    TextView mProfileNameTxt;
    @BindView(R.id.user_avatar_img)
    ImageView mUserAvatarImg;
    @BindView(R.id.user_phone_et)
    EditText mUserPhoneEt;
    @BindView(R.id.user_full_name_et)
    EditText mUserFullNameEt;
    @BindView(R.id.profile_name_wrapper)
    LinearLayout mProfileNameWrapper;
    @BindView(R.id.address_list)
    RecyclerView mAddressList;
    @BindView(R.id.add_address_btn)
    Button mAddAddressBtn;
    @BindView(R.id.notification_order_sw)
    SwitchCompat mNotificationOrderSw;
    @BindView(R.id.notification_promo_sw)
    SwitchCompat mNotificationPromoSw;

    private AccountScreen mScreen;
    private UserDto mUserDto;
    private TextWatcher mWatcher;
    private AddressesAdapter mAdapter;
    private Uri mAvatarUri;

    public AccountView(Context context) {
        this(context, null);
    }

    public AccountView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AccountView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
            DaggerService.<AccountScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    public AddressesAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    private void showViewFromState() {
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            showPreviewState();
        } else {
            showEditState();
        }
    }

    public void initView() {
        showViewFromState();
        mAdapter = new AddressesAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mAddressList.setLayoutManager(layoutManager);
        mAddressList.setAdapter(mAdapter);
        initSwipe();
    }

    public void initSettings(UserSettingDto settingDto){
        CompoundButton.OnCheckedChangeListener listener = (compoundButton, b) -> mPresenter.switchSettings();
        mNotificationOrderSw.setChecked(settingDto.isOrderNotification());
        mNotificationPromoSw.setChecked(settingDto.isPromoNotification());
        mNotificationOrderSw.setOnCheckedChangeListener(listener);
        mNotificationPromoSw.setOnCheckedChangeListener(listener);
    }

    private void initSwipe() {
        ItemSwipeCallBack swipeCallBack = new ItemSwipeCallBack(getContext(), 0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    showRemoveAddressDialog(position);
                } else {
                    showEditAddressDialog(position);
                }
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeCallBack);
        itemTouchHelper.attachToRecyclerView(mAddressList);
    }

    private void showEditAddressDialog(int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        dialogBuilder
                .setTitle("Перейти к редактированию адреса")
                .setMessage("Вы уверены, что хотите редактировать данный адресс?")
                .setPositiveButton("Редактировать", (dialogInterface, i) -> mPresenter.editAddress(position))
                .setNegativeButton("Отмена", (dialogInterface, i) -> dialogInterface.cancel())
                .setOnCancelListener(dialogInterface -> mAdapter.notifyDataSetChanged())
                .show();
    }

    private void showRemoveAddressDialog(int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        dialogBuilder
                .setTitle("Удалить адресс?")
                .setMessage("Вы уверены, что хотите удалить данный адресс из списка?")
                .setPositiveButton("Удалить", (dialogInterface, i) -> mPresenter.removeAddress(position))
                .setNegativeButton("Отмена", (dialogInterface, i) -> dialogInterface.cancel())
                .setOnCancelListener(dialogInterface -> mAdapter.notifyDataSetChanged())
                .show();
    }

    /*private void initSettings() {
        mNotificationOrderSw.setChecked(mUserDto.isOrderNotification());
        mNotificationOrderSw.setOnCheckedChangeListener((compoundButton, b) -> mPresenter.switchOrder(b));
        mNotificationPromoSw.setChecked(mUserDto.isPromoNotification());
        mNotificationPromoSw.setOnCheckedChangeListener((compoundButton, b) -> mPresenter.switchPromo(b));
    }

    private void loadAvatar() {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        final int size = (int) (135 * scale);
        mPicasso.load(mUserDto.getAvatar())
                .resize(size, size)
                .transform(new CircleTransform())
                .into(mUserAvatarImg);
    }*/

    //region IAccountView

    @Override
    public void changeState() {
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            mScreen.setCustomState(EDIT_STATE);
        } else {
            mScreen.setCustomState(PREVIEW_STATE);
        }
        showViewFromState();
    }

    @Override
    public void showEditState() {
        mWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mProfileNameTxt.setText(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        mUserFullNameEt.addTextChangedListener(mWatcher);
        mProfileNameWrapper.setVisibility(VISIBLE);
        mUserPhoneEt.setFocusable(true);
        mUserPhoneEt.setFocusableInTouchMode(true);
        mUserPhoneEt.setEnabled(true);
        mPicasso.load(R.drawable.ic_add_white)
                .error(R.drawable.ic_add_white)
                .into(mUserAvatarImg);
    }

    @Override
    public void showPreviewState() {
        mProfileNameWrapper.setVisibility(GONE);
        mUserPhoneEt.setFocusable(false);
        mUserPhoneEt.setFocusableInTouchMode(false);
        mUserPhoneEt.setEnabled(false);
        mUserFullNameEt.removeTextChangedListener(mWatcher);
        if (mAvatarUri != null){
            insertAvatar();
        }
    }

    @Override
    public void showPhotoSourceDialog() {
        String[] source = {"Загрузить из галереи", "Сделать фото", "Отмена"};
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Установить фото");
        alertDialog.setItems(source, (dialogInterface, i) -> {
            switch (i) {
                case 0:
                    mPresenter.chooseGallery();
                    break;
                case 1:
                    mPresenter.chooseCamera();
                    break;
                case 2:
                    dialogInterface.cancel();
                    break;

            }
        });
        alertDialog.show();
    }

    @Override
    public String getUserName() {
        return String.valueOf(mUserFullNameEt.getText());
    }

    @Override
    public String getPhoneName() {
        return String.valueOf(mUserPhoneEt.getText());
    }

    @Override
    public boolean viewOnBackPressed() {
        if (mScreen.getCustomState() == EDIT_STATE) {
            mPresenter.switchViewState();
            return true;
        } else {
            return false;
        }
    }

    public UserSettingDto getSettings() {
        return new UserSettingDto(mNotificationOrderSw.isChecked(), mNotificationPromoSw.isChecked());
    }

    public void updateAvatarPhoto(Uri uri) {
        mAvatarUri = uri;
        insertAvatar();
    }

    private void insertAvatar() {
        mPicasso.load(mAvatarUri)
                .resize(140, 140)
                .centerCrop()
                .transform(new CircleTransform())
                .into(mUserAvatarImg);
    }

    public UserInfoDto getUserProfileInfo() {
        return new UserInfoDto(mUserFullNameEt.getText().toString(),
                mUserPhoneEt.getText().toString(),
                String.valueOf(mAvatarUri));
    }

    public void updateProfileInfo(UserInfoDto userInfoDto) {
        mProfileNameTxt.setText(userInfoDto.getName());
        mUserFullNameEt.setText(userInfoDto.getName());
        mUserPhoneEt.setText(userInfoDto.getPhone());
        if (mScreen.getCustomState() == PREVIEW_STATE){
            mAvatarUri = Uri.parse(userInfoDto.getAvatar());
            insertAvatar();
        }
    }

    //endregion

    //region Events

    @OnClick(R.id.collapsing_toolbar)
    void clickToolbar() {
        mPresenter.switchViewState();
    }

    @OnClick(R.id.add_address_btn)
    void clickAddAddress() {
        mPresenter.clickOnAddress();
    }

    @OnClick(R.id.user_avatar_img)
    void clickChangeAvatar() {
        if (mScreen.getCustomState() == EDIT_STATE){
            mPresenter.takePhoto();
        }
    }

    //endregion

}
