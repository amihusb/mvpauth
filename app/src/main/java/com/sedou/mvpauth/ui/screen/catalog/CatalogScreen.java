package com.sedou.mvpauth.ui.screen.catalog;

import android.content.Context;
import android.os.Bundle;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.realm.ProductRealm;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.di.scopes.DaggerScope;
import com.sedou.mvpauth.flow.AbstractScreen;
import com.sedou.mvpauth.flow.Screen;
import com.sedou.mvpauth.mvp.models.CatalogModel;
import com.sedou.mvpauth.mvp.presenters.AbstractPresenter;
import com.sedou.mvpauth.mvp.presenters.ICatalogPresenter;
import com.sedou.mvpauth.mvp.presenters.MenuItemHolder;
import com.sedou.mvpauth.mvp.presenters.RootPresenter;
import com.sedou.mvpauth.ui.activities.RootActivity;
import com.sedou.mvpauth.ui.screen.auth.AuthScreen;
import com.sedou.mvpauth.ui.screen.product.ProductScreen;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Subscriber;
import rx.Subscription;

@Screen(R.layout.screen_catalog)
public class CatalogScreen extends AbstractScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerCatalogScreen_Component
                .builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    //region DI

    @dagger.Module
    public class Module {

        @Provides
        @DaggerScope(CatalogScreen.class)
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }

        @Provides
        @DaggerScope(CatalogScreen.class)
        CatalogPresenter provideCatalogPresenter() {
            return new CatalogPresenter();
        }

    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DaggerScope(CatalogScreen.class)
    public interface Component {
        void inject(CatalogPresenter presenter);

        void inject(CatalogView view);

        CatalogModel getCatalogModel();

        Picasso getPicasso();

        RootPresenter getRootPresenter();
    }
    //endregion

    //region Presenter
    public class CatalogPresenter extends AbstractPresenter<CatalogView, CatalogModel> implements ICatalogPresenter {

        private int lastPagerPosition;

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            mCompositeSubscription.add(subscribeOnProductRealmObs());
        }

        @Override
        public void dropView(CatalogView view) {
            lastPagerPosition = getView().getCurrentPagerPosition();
            super.dropView(view);
        }

        @Override
        protected void initializeDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initializeActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setTitle("Каталог")
                    .addAction(new MenuItemHolder("В корзину", R.drawable.ic_shopping_basket, menuItem -> {
                        if (getRootView() != null) {
                            getRootView().showMessage("Перейти в корзину");
                        }
                        return true;
                    }))
                    .build();
        }

        @Override
        public void clickOnBuyButton(int position) {
            if (getView() != null) {
                if (checkUserAuth() && getRootView() != null) {
                    getRootView().showMessage("Товар "
                            + mModel.getProductList().get(position).getProductName()
                            + " успешно добавлен в корзину!");
                } else {
                    Flow.get(getView()).set(new AuthScreen());
                }
            }
        }

        private Subscription subscribeOnProductRealmObs() {
            if (getRootView() != null) {
                getRootView().showLoad();
            }
            return mModel.getProductObs()
                    .subscribe(new RealmSubscriber());
        }

        @Override
        public boolean checkUserAuth() {
            return mModel.isUserAuth();
        }

        private class RealmSubscriber extends Subscriber<ProductRealm> {

            CatalogAdapter mAdapter = getView().getAdapter();

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                if (getRootView() != null) {
                    getRootView().showError(e);
                }
            }

            @Override
            public void onNext(ProductRealm productRealm) {
                mAdapter.addItem(productRealm);
                if (mAdapter.getCount() - 1 == lastPagerPosition && getRootView() != null) {
                    getRootView().hideLoad();
                    getView().showCatalogView();
                }
            }
        }

    }

    //endregion

    public static class Factory {
        public static Context createProductContext(ProductRealm product, Context parentContext) {
            MortarScope parentScope = MortarScope.getScope(parentContext);
            MortarScope childScope;
            ProductScreen screen = new ProductScreen(product);
            String scopeName = String.format(Locale.ENGLISH, "%s_%s", screen.getScopeName(), product.getId());
            childScope = parentScope.findChild(scopeName);
            if (childScope == null) {
                childScope = parentScope
                        .buildChild()
                        .withService(DaggerService.SERVICE_NAME,
                                screen.createScreenComponent(DaggerService.<CatalogScreen.Component>getDaggerComponent(parentContext)))
                        .build(scopeName);
            }
            return childScope.createContext(parentContext);
        }
    }
}
