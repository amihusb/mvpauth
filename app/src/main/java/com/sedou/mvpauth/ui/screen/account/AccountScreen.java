package com.sedou.mvpauth.ui.screen.account;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.dto.ActivityResultDto;
import com.sedou.mvpauth.data.storage.dto.UserAddressDto;
import com.sedou.mvpauth.data.storage.dto.UserInfoDto;
import com.sedou.mvpauth.data.storage.dto.UserSettingDto;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.di.scopes.AccountScope;
import com.sedou.mvpauth.flow.AbstractScreen;
import com.sedou.mvpauth.flow.Screen;
import com.sedou.mvpauth.mvp.models.AccountModel;
import com.sedou.mvpauth.mvp.presenters.IAccountPresenter;
import com.sedou.mvpauth.mvp.presenters.RootPresenter;
import com.sedou.mvpauth.mvp.presenters.SubscribePresenter;
import com.sedou.mvpauth.mvp.views.IRootView;
import com.sedou.mvpauth.ui.activities.RootActivity;
import com.sedou.mvpauth.ui.screen.address.AddressScreen;
import com.sedou.mvpauth.utils.ConstantManager;
import com.sedou.mvpauth.utils.FileFactory;

import java.io.File;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Observable;
import rx.Subscription;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCustomState = AccountView.PREVIEW_STATE;

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int customState) {
        mCustomState = customState;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAccountScreen_Component
                .builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    //region DI

    @dagger.Module
    public class Module {

        @Provides
        @AccountScope
        AccountPresenter provideAccountPresenter() {
            return new AccountPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AccountScope
    public interface Component {
        void inject(AccountPresenter presenter);

        void inject(AccountView view);

        RootPresenter getRootPresenter();

        AccountModel getAccountModel();
    }
    //endregion

    //region Presenter

    public class AccountPresenter extends SubscribePresenter<AccountView> implements IAccountPresenter {

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        AccountModel mAccountModel;

        private Uri mAvatarUri;
        private Subscription mAddressSub;
        private Subscription mSettingSub;
        private Subscription mActivityResultSub;
        private Subscription mUserInfoSub;
        private File mPhotoFile;

        //region Life Cycle

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
            subscribeOnActivityResultObs();
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().initView();
            }
            subscribeOnAddressesObs();
            subscribeOnSettingsObs();
            subscribeOnUserInfoObs();
        }

        @Override
        protected void onSave(Bundle outState) {
            super.onSave(outState);
            mAddressSub.unsubscribe();
            mSettingSub.unsubscribe();
            mUserInfoSub.unsubscribe();
        }

        @Override
        protected void onExitScope() {
            mActivityResultSub.unsubscribe();
            super.onExitScope();
        }

        //endregion

        //region subscription

        private void updateListView() {
            if (getView() != null) {
                getView().getAdapter().reloadAdapter();
                subscribeOnAddressesObs();
            }
        }

        private void subscribeOnAddressesObs() {
            mAddressSub = subscribe(mAccountModel.getAddressObs(), new ViewSubscriber<UserAddressDto>() {
                @Override
                public void onNext(UserAddressDto addressDto) {
                    if (getView() != null) {
                        getView().getAdapter().addItem(addressDto);
                    }
                }
            });
        }

        private void subscribeOnSettingsObs() {
            mSettingSub = subscribe(mAccountModel.getUserSettingsObs(), new ViewSubscriber<UserSettingDto>() {
                @Override
                public void onNext(UserSettingDto userSettingDto) {
                    if (getView() != null) {
                        getView().initSettings(userSettingDto);
                    }
                }
            });
        }

        private void subscribeOnActivityResultObs() {
            Observable<ActivityResultDto> activityResultObs = mRootPresenter.getActivityResultDtoObs()
                    .filter(activityResultDto -> activityResultDto.getResultCode() == Activity.RESULT_OK);
            mActivityResultSub = subscribe(activityResultObs, new ViewSubscriber<ActivityResultDto>() {
                @Override
                public void onNext(ActivityResultDto activityResultDto) {
                    handleActivityResult(activityResultDto);
                }
            });
        }

        // TODO: 03.01.2017 remake with rx
        private void handleActivityResult(ActivityResultDto activityResultDto) {
            switch ( (activityResultDto.getRequestCode())){
                case ConstantManager.REQUEST_PROFILE_PHOTO_PICKER:
                    if (activityResultDto.getIntent() != null){
                        String photoUri = activityResultDto.getIntent().getData().toString();
                        getView().updateAvatarPhoto(Uri.parse(photoUri));
                    }
                    break;
                case ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA:
                    if (mPhotoFile != null){
                        getView().updateAvatarPhoto(Uri.fromFile(mPhotoFile));
                    }
            }
        }

        private void subscribeOnUserInfoObs() {
            mUserInfoSub = subscribe(mAccountModel.getUserInfoObs(), new ViewSubscriber<UserInfoDto>() {
                @Override
                public void onNext(UserInfoDto userInfoDto) {
                    if (getView() != null) {
                        getView().updateProfileInfo(userInfoDto);
                    }
                }
            });
        }

        //endregion


        @Override
        public void clickOnAddress() {
            if (getView() != null) {
                Flow.get(getView()).set(new AddressScreen(null));
            }
        }

        @Override
        public void switchViewState() {
            if (getCustomState() == AccountView.EDIT_STATE && getView() != null) {
                mAccountModel.saveProfileInfo(getView().getUserProfileInfo());
            }
            if (getView() != null) {
                getView().changeState();
            }
        }

        @Override
        public void takePhoto() {
            if (getView() != null) {
                getView().showPhotoSourceDialog();
            }
        }

        //region CAMERA

        @Override
        public void chooseCamera() {
            if (getRootView() != null) {
                String [] permissions = {CAMERA, WRITE_EXTERNAL_STORAGE};
                if (mRootPresenter.checkPermissionsAndRequestIfNotGranted(permissions,
                        ConstantManager.REQUEST_PERMISSION_CAMERA)){
                    mPhotoFile = FileFactory.createFileForPhoto(getView().getContext());
                    if (mPhotoFile == null) {
                        getRootView().showMessage("Фотография не может быть создана");
                        return;
                    }
                    takePhotoFromCamera();
                }
            }
        }

        private void takePhotoFromCamera() {
            Uri uriForFile = FileProvider.getUriForFile(((RootActivity) getRootView()),
                    ConstantManager.FILE_PROVIDER_AUTHORITY,
                    mPhotoFile);
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriForFile);
            ((RootActivity) getRootView()).startActivityForResult(takePictureIntent,
                    ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA);
        }

        //endregion

        //region GALLERY

        @Override
        public void chooseGallery() {
            if (getRootView() != null) {
                String[] permission = {READ_EXTERNAL_STORAGE};
                if (mRootPresenter.checkPermissionsAndRequestIfNotGranted(permission,
                        ConstantManager.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE)){
                    takePhotoFromGallery();
                }
            }
        }

        private void takePhotoFromGallery() {
            Intent intent = new Intent();
            if (Build.VERSION.SDK_INT < 19){
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
            } else {
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
            }
            ((RootActivity) getRootView()).startActivityForResult(intent, ConstantManager.REQUEST_PROFILE_PHOTO_PICKER);
        }

        //endregion

        @Override
        public void removeAddress(int position) {
            mAccountModel.removeAddress(mAccountModel.getAddressFromPosition(position));
            updateListView();
        }

        @Override
        public void editAddress(int position) {
            if (getView() != null) {
                Flow.get(getView()).set(new AddressScreen(mAccountModel.getAddressFromPosition(position)));
            }
        }

        @Nullable
        @Override
        protected IRootView getRootView() {
            return mRootPresenter.getRootView();
        }

        public void switchSettings() {
            if (getView() != null) {
                mAccountModel.saveSettings(getView().getSettings());
            }
        }
    }

    //endregion
}
