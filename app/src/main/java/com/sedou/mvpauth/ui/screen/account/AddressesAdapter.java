package com.sedou.mvpauth.ui.screen.account;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.dto.UserAddressDto;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddressesAdapter extends Adapter<AddressesAdapter.ViewHolder> {

    private List<UserAddressDto> mUserAddresses = new ArrayList<>();

    public void addItem(UserAddressDto addressDto){
        mUserAddresses.add(addressDto);
        notifyDataSetChanged();
    }

    public void reloadAdapter(){
        mUserAddresses.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_addess, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UserAddressDto address = mUserAddresses.get(position);

        holder.mLabelAddressTxt.setText(address.getName());
        holder.mAddressTxt.setText(address.toString());
        holder.mCommentTxt.setText(address.getComment());
    }

    @Override
    public int getItemCount() {
        return mUserAddresses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.label_address_txt)
        TextView mLabelAddressTxt;
        @BindView(R.id.address_txt)
        TextView mAddressTxt;
        @BindView(R.id.comment_txt)
        TextView mCommentTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
