package com.sedou.mvpauth.ui.screen.address;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.dto.UserAddressDto;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.mvp.views.IAddressView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressView extends RelativeLayout implements IAddressView {

    @BindView(R.id.address_name_et)
    EditText mAddressNameEt;
    @BindView(R.id.street_et)
    EditText mStreetEt;
    @BindView(R.id.number_home_et)
    EditText mNumberHomeEt;
    @BindView(R.id.number_apartment_et)
    EditText mNumberApartmentEt;
    @BindView(R.id.number_floor_et)
    EditText mNumberFloorEt;
    @BindView(R.id.comment_et)
    EditText mCommentEt;
    @BindView(R.id.add_address_btn)
    Button mAddAddressBtn;

    @Inject
    AddressScreen.AddressPresenter mPresenter;

    private int mAddressId;

    public AddressView(Context context) {
        this(context, null);
    }

    public AddressView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AddressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            DaggerService.<AddressScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    //region IAddressView

    public void initView(@Nullable UserAddressDto addressDto) {
        if (addressDto != null) {
            mAddressId = addressDto.getId();
            mAddressNameEt.setText(addressDto.getName());
            mStreetEt.setText(addressDto.getStreet());
            mNumberHomeEt.setText(addressDto.getHouse());
            mNumberApartmentEt.setText(addressDto.getApartment());
            mNumberFloorEt.setText(String.valueOf(addressDto.getFloor()));
            mCommentEt.setText(addressDto.getComment());
            mAddAddressBtn.setText(R.string.save);
        }
    }

    @Override
    public void showInputError() {
        // TODO: 04.12.2016 implement this
    }

    @Override
    public UserAddressDto getUserAddress() {
        return new UserAddressDto(mAddressId,
                String.valueOf(mAddressNameEt.getText()),
                String.valueOf(mStreetEt.getText()),
                String.valueOf(mNumberHomeEt.getText()),
                String.valueOf(mNumberApartmentEt.getText()),
                Integer.parseInt(String.valueOf(mNumberFloorEt.getText())),
                String.valueOf(mCommentEt.getText()));
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion

    //region Events

    @OnClick(R.id.add_address_btn)
    void clickAddAddress() {
        mPresenter.clickOnAddAddress();
    }
    //endregion
}
