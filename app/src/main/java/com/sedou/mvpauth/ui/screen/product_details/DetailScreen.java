package com.sedou.mvpauth.ui.screen.product_details;

import android.os.Bundle;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.data.storage.realm.ProductRealm;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.di.scopes.DaggerScope;
import com.sedou.mvpauth.flow.AbstractScreen;
import com.sedou.mvpauth.flow.Screen;
import com.sedou.mvpauth.mvp.models.DetailModel;
import com.sedou.mvpauth.mvp.presenters.AbstractPresenter;
import com.sedou.mvpauth.mvp.presenters.MenuItemHolder;
import com.sedou.mvpauth.mvp.presenters.RootPresenter;
import com.sedou.mvpauth.ui.screen.catalog.CatalogScreen;
import com.squareup.picasso.Picasso;

import dagger.Provides;
import flow.TreeKey;
import mortar.MortarScope;

@Screen(R.layout.screen_detail)
public class DetailScreen extends AbstractScreen<CatalogScreen.Component> implements TreeKey {

    private final ProductRealm mProductRealm;

    public DetailScreen(ProductRealm product) {
        mProductRealm = product;
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerDetailScreen_Component
                .builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new CatalogScreen();
    }

    //region DI

    @dagger.Module
    public class Module {

        @Provides
        @DaggerScope(DetailScreen.class)
        DetailPresenter provideDetailPresenter() {
            return new DetailPresenter(mProductRealm);
        }

        @Provides
        @DaggerScope(DetailScreen.class)
        DetailModel provideDetailModel() {
            return new DetailModel();
        }
    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    @DaggerScope(DetailScreen.class)
    public interface Component {
        void inject(DetailPresenter presenter);
        void inject(DetailView view);

        DetailModel getDetailModel();
        RootPresenter getRootPresenter();
        Picasso getPicasso();
    }

    public class DetailPresenter extends AbstractPresenter<DetailView, DetailModel> {

        private final ProductRealm mProduct;

        public DetailPresenter(ProductRealm productRealm) {
            mProduct = productRealm;
        }

        @Override
        protected void initializeActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setTitle(mProduct.getProductName())
                    .setBackArrow(true)
                    .addAction(new MenuItemHolder("В корзину", R.drawable.ic_shopping_basket, menuItem -> {
                        if (getRootView() != null) getRootView().showMessage("Перейти в корзину");
                        return true;
                    }))
                    .setViewPager(getView().getViewPager())
                    .build();
        }

        @Override
        protected void initializeDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().initView(mProduct);
        }
    }

    //endregion
}
