package com.sedou.mvpauth.ui.screen.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sedou.mvpauth.R;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.di.scopes.AuthScope;
import com.sedou.mvpauth.flow.AbstractScreen;
import com.sedou.mvpauth.flow.Screen;
import com.sedou.mvpauth.mvp.models.AuthModel;
import com.sedou.mvpauth.mvp.presenters.IAuthPresenter;
import com.sedou.mvpauth.mvp.presenters.RootPresenter;
import com.sedou.mvpauth.mvp.views.IRootView;
import com.sedou.mvpauth.ui.activities.RootActivity;
import com.sedou.mvpauth.ui.activities.SplashActivity;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;

@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCustomState = AuthView.IDLE_STATE;

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int state) {
        mCustomState = state;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentRootComponent) {
        return DaggerAuthScreen_Component
                .builder()
                .rootComponent(parentRootComponent)
                .module(new Module())
                .build();
    }

    //region DI
    @dagger.Module
    public class Module {

        @Provides
        @AuthScope
        AuthPresenter providePresenter() {
            return new AuthPresenter();
        }

        @Provides
        @AuthScope
        AuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AuthScope
    interface Component {
        void inject(AuthPresenter presenter);

        void inject(AuthView view);
    }
    //endregion

    //region Presenter
    public class AuthPresenter extends ViewPresenter<AuthView> implements IAuthPresenter {

        @Inject
        AuthModel mAuthModel;
        @Inject
        RootPresenter mRootPresenter;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if (getView() != null) {
                if (checkUserAuth()) {
                    getView().hideLoginBtn();
                } else {
                    getView().showLoginBtn();
                }
            }
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getRootView();
        }

        @Override
        public void clickOnLogin() {
            if (getView() != null && getRootView() != null) {
                if (getView().isIdle()) {
                    getView().setCustomState(AuthView.LOGIN_STATE);
                } else {
                    // TODO: 20.10.2016 auth user
                    mAuthModel.loginUser(getView().getUserEmail(), getView().getUserPassword());
                    getRootView().showMessage("request for user auth");
                }
            }
        }

        @Override
        public void clickOnFb() {
            if (getRootView() != null) {
                getRootView().showMessage("clickOnFb");
            }
        }

        @Override
        public void clickOnVk() {
            if (getRootView() != null) {
                getRootView().showMessage("clickOnVk");
            }
        }

        @Override
        public void clickOnTwitter() {
            if (getRootView() != null) {
                getRootView().showMessage("clickOnTwitter");
            }
        }

        @Override
        public void clickOnShowCatalog() {
            if (getView() != null && getRootView() != null) {
                getRootView().showMessage("Показать каталог");

                if (getRootView() instanceof SplashActivity){
                    ((SplashActivity) getRootView()).startRootActivity();
                } else {
                    // TODO: 03.12.2016 show catalog screen
                }
            }
        }

        @Override
        public boolean checkUserAuth() {
            return mAuthModel.isUserAuth();
        }
    }
    //endregion
}
