package com.sedou.mvpauth.di.components;

import com.sedou.mvpauth.data.managers.DataManager;
import com.sedou.mvpauth.di.modules.LocalModule;
import com.sedou.mvpauth.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
