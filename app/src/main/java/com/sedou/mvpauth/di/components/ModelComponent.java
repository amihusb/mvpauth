package com.sedou.mvpauth.di.components;

import com.sedou.mvpauth.di.modules.ModelModule;
import com.sedou.mvpauth.mvp.models.AbstractModel;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
