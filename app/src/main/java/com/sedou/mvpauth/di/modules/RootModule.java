package com.sedou.mvpauth.di.modules;

import com.sedou.mvpauth.di.scopes.RootScope;
import com.sedou.mvpauth.mvp.models.AccountModel;
import com.sedou.mvpauth.mvp.presenters.RootPresenter;

import dagger.Provides;

@dagger.Module
public class RootModule {

    @Provides
    @RootScope
    RootPresenter provideRootPresenter(){
        return new RootPresenter();
    }

    @Provides
    @RootScope
    AccountModel provideAccountModel() {
        return new AccountModel();
    }
}
