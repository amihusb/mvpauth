package com.sedou.mvpauth.di.components;

import com.sedou.mvpauth.di.modules.PicassoCacheModule;
import com.sedou.mvpauth.di.scopes.RootScope;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {
    Picasso getPicasso();
}
