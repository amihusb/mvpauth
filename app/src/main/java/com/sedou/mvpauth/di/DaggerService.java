package com.sedou.mvpauth.di;

import android.content.Context;
import android.support.annotation.Nullable;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DaggerService {

    private static Map<Class, Object> sComponentMap = new HashMap<>();

    public static String SERVICE_NAME = "MY_DAGGER_SERVICE";

    @SuppressWarnings("unchecked")
    public static <T> T getDaggerComponent(Context context){
        //noinspection ResourceType
        return (T) context.getSystemService(SERVICE_NAME);
    }

    // TODO: 02.12.2016 FIX ME PLZ
    public static void registerComponent(Class componentClass, Object daggerComponent){
        sComponentMap.put(componentClass, daggerComponent);
    }

    @Nullable
    @SuppressWarnings("unckecked")
    public static <T> T getComponent(Class<T> componentClass){
        Object component = sComponentMap.get(componentClass);
        //noinspection unchecked
        return (T) component;
    }

    public static void unregisterScope(Class<? extends Annotation> scopeAnotation){
        Iterator<Map.Entry<Class, Object>> iterator = sComponentMap.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<Class, Object> entry = iterator.next();
            if (entry.getKey().isAnnotationPresent(scopeAnotation)){
                iterator.remove();
            }
        }
    }

    public static <T> T createDaggerComponent(Class<T> componentClass, Object... dependency){

        String fqn = componentClass.getName();
        String packageName = componentClass.getPackage().getName();
        String simpleName = fqn.substring(packageName.length() + 1);
        String generatedName = (packageName + ".Dagger" + simpleName).replace('$', '_');

        try {
            Class<?> generatedClass = Class.forName(generatedName);
            Object builder = generatedClass.getMethod("builder").invoke(null);

            for (Method method : builder.getClass().getMethods()) {
                Class<?>[] params = method.getParameterTypes();
                if (params.length == 1) {
                    Class<?> dependencyClass = params[0];
                    for (Object dependence : dependency) {
                        if (dependencyClass.isAssignableFrom(dependency.getClass())) {
                            method.invoke(builder, dependency);
                            break;
                        }
                    }
                }
            }
            //noinspection unchecked
            return (T) builder.getClass().getMethod("build").invoke(builder);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
