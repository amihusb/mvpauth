package com.sedou.mvpauth.di.components;

import android.content.Context;

import com.sedou.mvpauth.di.modules.AppModule;

import dagger.Component;

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
