package com.sedou.mvpauth.mvp.views;

public interface ICatalogView extends IView {
    void showCatalogView();
    void updateProductCounter();
}