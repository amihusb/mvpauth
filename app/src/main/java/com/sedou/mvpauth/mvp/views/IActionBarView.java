package com.sedou.mvpauth.mvp.views;

import android.support.v4.view.ViewPager;

import com.sedou.mvpauth.mvp.presenters.MenuItemHolder;

import java.util.List;

public interface IActionBarView {
    void setTitle(CharSequence title);
    void setVisable(boolean visible);
    void setBackArrow(boolean enable);
    void setMenuItem(List<MenuItemHolder> items);
    void setTabLayout(ViewPager pager);
    void removeTabLayout();
}
