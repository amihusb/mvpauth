package com.sedou.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import com.sedou.mvpauth.mvp.views.IAuthView;

public interface IAuthPresenter {

    void clickOnLogin();
    void clickOnFb();
    void clickOnVk();
    void clickOnTwitter();
    void clickOnShowCatalog();

    boolean checkUserAuth();
}