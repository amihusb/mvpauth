package com.sedou.mvpauth.mvp.views;

public interface IAuthView extends IView {

    void showLoginBtn();
    void hideLoginBtn();
    void showCatalogScreen();
    /*void isUserAuth(boolean condition);
    */

    String getUserEmail();
    String getUserPassword();

    boolean isIdle();

    void setCustomState(int state);
}