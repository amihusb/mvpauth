package com.sedou.mvpauth.mvp.views;

import com.sedou.mvpauth.data.storage.dto.UserAddressDto;

public interface IAddressView extends IView{
    void showInputError();
    UserAddressDto getUserAddress();
}
