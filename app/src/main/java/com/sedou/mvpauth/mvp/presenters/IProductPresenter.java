package com.sedou.mvpauth.mvp.presenters;

public interface IProductPresenter {
    void clickOnPlus();
    void clickOnMinus();
}