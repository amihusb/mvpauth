package com.sedou.mvpauth.mvp.models;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.sedou.mvpauth.data.storage.dto.ProductDto;
import com.sedou.mvpauth.data.storage.dto.ProductLocalInfo;
import com.sedou.mvpauth.data.storage.realm.ProductRealm;

import java.util.List;

import rx.Observable;

public class CatalogModel extends AbstractModel {

    public List<ProductDto> getProductList() {
        return mDataManager.getMockProductList();
    }

    public boolean isUserAuth() {
        return mDataManager.isUserAuth();
    }

    public ProductDto getProductById(int productId) {
        // TODO: 27.10.2016 get product from datamanager
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product) {
        mDataManager.updateProduct(product);
    }

    public Observable<ProductRealm> getProductObs() {
        Observable<ProductRealm> disk = fromDisc();
        Observable<ProductRealm> network = fromNetwork();
        return Observable.mergeDelayError(disk, network)
                .distinct(ProductRealm::getId);
    }

    @RxLogObservable
    public Observable<ProductRealm> fromNetwork() {
        return mDataManager.getProductsObsFromNetwork();
    }

    @RxLogObservable
    public Observable<ProductRealm> fromDisc() {
        return mDataManager.getProductObsFromRealm(); /*Observable.defer(() -> {
            List<ProductDto> discData = mDataManager.fromDisk();
            return discData == null ?
                    Observable.empty() :
                    Observable.from(discData);
        });*/
    }

    public void updateProductLocalInfo(ProductLocalInfo pli) {
        // TODO: 08.01.2017 save in local db
        //mDataManager.getPreferencesManager()
    }
}