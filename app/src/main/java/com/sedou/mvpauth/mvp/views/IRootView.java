package com.sedou.mvpauth.mvp.views;

import android.support.annotation.Nullable;

import com.sedou.mvpauth.data.storage.dto.UserInfoDto;

public interface IRootView extends IView {
    void showMessage(String message);
    void showError(Throwable e);
    void showLoad();
    void hideLoad();

    @Nullable
    IView getCurrentScreen();

    void initDrawer(UserInfoDto userInfoDto);
}
