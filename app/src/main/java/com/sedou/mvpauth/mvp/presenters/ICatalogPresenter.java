package com.sedou.mvpauth.mvp.presenters;


public interface ICatalogPresenter {
    void clickOnBuyButton(int position);
    boolean checkUserAuth();
}