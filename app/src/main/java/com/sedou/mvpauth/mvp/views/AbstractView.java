package com.sedou.mvpauth.mvp.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.sedou.mvpauth.mvp.presenters.AbstractPresenter;

import javax.inject.Inject;

import butterknife.ButterKnife;

public abstract class AbstractView<P extends AbstractPresenter> extends FrameLayout implements IView {

    @Inject
    protected P mPresenter;

    public AbstractView(Context context) {
        this(context, null);
    }

    public AbstractView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AbstractView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            initializeDagger(context);
        }
    }

    protected abstract void initializeDagger(Context context);

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this); // или data binding
    }
}
