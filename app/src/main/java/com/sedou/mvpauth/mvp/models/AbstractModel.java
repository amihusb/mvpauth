package com.sedou.mvpauth.mvp.models;

import com.sedou.mvpauth.data.managers.DataManager;
import com.sedou.mvpauth.di.DaggerService;
import com.sedou.mvpauth.di.components.DaggerModelComponent;
import com.sedou.mvpauth.di.components.ModelComponent;
import com.sedou.mvpauth.di.modules.ModelModule;

import javax.inject.Inject;

public abstract class AbstractModel {

    @Inject
    DataManager mDataManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(ModelComponent.class, component);
        }
        component.inject(this);
    }

    private ModelComponent createDaggerComponent() {
        return DaggerModelComponent
                .builder()
                .modelModule(new ModelModule())
                .build();
    }


}
