package com.sedou.mvpauth.mvp.views;

public interface IView {

    boolean viewOnBackPressed();
}