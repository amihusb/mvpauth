package com.sedou.mvpauth.mvp.views;

import com.sedou.mvpauth.data.storage.dto.ProductDto;

public interface IProductView extends IView {
    void showProductView(ProductDto product);
    void updateProductCountView(ProductDto product);
}