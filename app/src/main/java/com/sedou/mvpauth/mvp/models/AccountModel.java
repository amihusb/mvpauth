package com.sedou.mvpauth.mvp.models;

import android.net.Uri;

import com.sedou.mvpauth.data.managers.DataManager;
import com.sedou.mvpauth.data.managers.PreferencesManager;
import com.sedou.mvpauth.data.storage.dto.UserAddressDto;
import com.sedou.mvpauth.data.storage.dto.UserDto;
import com.sedou.mvpauth.data.storage.dto.UserInfoDto;
import com.sedou.mvpauth.data.storage.dto.UserSettingDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

import static com.sedou.mvpauth.data.managers.PreferencesManager.*;

public class AccountModel extends AbstractModel {

    private PublishSubject<UserInfoDto> mUserInfoObs = PublishSubject.create();

    public AccountModel() {
        mUserInfoObs.onNext(getUserProfileInfo());
    }

    //region Addresses

    public Observable<UserAddressDto> getAddressObs(){
        return Observable.from(getUserAddresses());
    }

    private List<UserAddressDto> getUserAddresses(){
        return mDataManager.getUserAddresses();
    }

    public void updateOrInsertAddress(UserAddressDto addressDto){
        mDataManager.updateOrInsertAddress(addressDto);
    }

    public void removeAddress(UserAddressDto addressDto){
        mDataManager.removeAddress(addressDto);
    }

    public UserAddressDto getAddressFromPosition(int position){
        return getUserAddresses().get(position);
    }

    //endregion

    //region Settings

    public Observable<UserSettingDto> getUserSettingsObs(){
        return Observable.just(getUserSettings());
    }

    private UserSettingDto getUserSettings(){
        Map<String, Boolean> map = mDataManager.getUserSettings();
        return new UserSettingDto(map.get(NOTIFICATION_ORDER_KEY),
                map.get(NOTIFICATION_PROMO_KEY));
    }

    public void saveSettings(UserSettingDto settings) {
        mDataManager.saveSettings(NOTIFICATION_ORDER_KEY, settings.isOrderNotification());
        mDataManager.saveSettings(NOTIFICATION_PROMO_KEY, settings.isPromoNotification());
    }
    //endregion

    //region User

    public void saveProfileInfo(UserInfoDto userInfo){
        mDataManager.saveProfileInfo(userInfo.getName(), userInfo.getPhone(), userInfo.getAvatar());
        mUserInfoObs.onNext(userInfo);
    }

    public UserInfoDto getUserProfileInfo(){
        Map<String, String> map = mDataManager.getUserProfileInfo();
        return new UserInfoDto(
                map.get(PROFILE_FULL_NAME_KEY),
                map.get(PROFILE_PHONE_KEY),
                map.get(PROFILE_AVATAR_KEY));
    }

    public Observable<UserInfoDto> getUserInfoObs(){
        return mUserInfoObs;
    }
    //endregion
}
